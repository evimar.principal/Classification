# -*- coding: utf-8 -*-
"""
Created on Thu Mar  2 12:04:55 2017

@author: evimar
"""

import numpy as np
import random
import sys
import matplotlib.pylab as plt
import csv

def signo(x, w):
    if x.dot(w).sum()>=0:
        return 1
    else:
        return -1
def print_graph(w):
    plt.plot(x[label==1], y[label==1], '.')
    plt.plot(x[label==-1], y[label==-1], '.')
    w1, w2, w3 = w
    plt.plot((0,-w3/w1),(-w3/w2,0))
    plt.show()
    
input_file, output_file = "input1.csv", "output1.csv" #sys.argv[1:]


Data=np.genfromtxt(input_file, delimiter=',')
#Data = pd.read_csv(input_file, delimiter=',')
x = Data[:, 0]
y = Data[:, 1]
label = Data[:, 2]

n = x.shape[0] #Number of samples

vector = np.c_[  Data[:,:2], np.ones(n) ]
n,d = vector.shape




#Inizialize w
w = np.zeros(d)

converge = False

while not converge:
    converge=True    
#    print("**** EVALUANDO ****", w)
    secuencia = [i for i in range(n)]
    for i in range(n):
        indice = random.choice(secuencia)
        secuencia.remove(indice)
        punto = vector[indice]
        if label[indice]*signo(punto, w) <=0:
            w = w + label[indice] * punto
            print_graph(w)
            with open(output_file, 'a', newline='') as f:
                a = csv.writer(f, delimiter=',')
                a.writerow(w)
#                f.write(str(w.tolist())[1:-1].replace(" ",""))
            converge=False
            break
    
    
    
    