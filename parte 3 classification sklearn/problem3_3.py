# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 10:38:41 2017

@author: evimar
"""
import numpy as np
import csv
import matplotlib.pylab as plt

from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.svm import SVC
from sklearn.metrics import classification_report


input_file, output_file = "input3.csv", "outpu3.csv"

#Data=np.genfromtxt(input_file, delimiter=',', names=True, dtype=(float, float, int))

with open(input_file, newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    header = reader.__next__()
    X = np.array(reader.__next__(), dtype=float)
    for row in reader:
        X = np.vstack((X,np.array(row, dtype=float)))
        
n,d = X.shape

x = X[:, 0]
y = X[:, 1]
label = X[:, 2]        

X_train, X_test, y_train, y_test = train_test_split(X[:,:2], label, test_size=0.4, random_state=1 )

#plt.plot(x[label==1], y[label==1],'.b')
#plt.plot(x[label==0], y[label==0],'.r')
#plt.show()
#print((label==0).sum()/len(label))
#
#plt.plot(X_fit[y_fit==1,0], X_fit[y_fit==1,1],'.b')
#plt.plot(X_fit[y_fit==0,0], X_fit[y_fit==0,1], '.r')
#plt.show()
#print((y_fit==0).sum()/len(y_fit))
#
#
#plt.plot(X_eval[y_eval==1,0],X_eval[y_eval==1,1],'.b')
#plt.plot(X_eval[y_eval==0,0],X_eval[y_eval==0,1],'.r')
#plt.show()
#print((y_eval==0).sum()/len(y_eval))


tuned_parameters = [{'kernel': ['linear'], 'C': [50, 0.1, 1, 5, 10, 0.50, 100]}]


clf = GridSearchCV(SVC(C=1), tuned_parameters, cv=5)
clf.fit(X_train, y_train)

print("Best parameters set found on development set:")
print()
print(clf.best_params_)
print()
print("Grid scores on development set:")
print()


print("Detailed classification report:")
print()
print("The model is trained on the full development set.")
print("The scores are computed on the full evaluation set.")
print()
y_true, y_pred = y_test, clf.predict(X_test)
print(classification_report(y_true, y_pred))
print()