# -*- coding: utf-8 -*-
"""
Created on Thu Mar  2 12:04:55 2017

@author: evimar
"""

import numpy as np
import sys
#import matplotlib.pylab as plt
import csv

def log_regresion(X, label, alpha, iteraciones):
    n,d  = X.shape
    bethas = np.zeros(d)
    
    for i in range(iteraciones):
        
        XBY = X.dot(bethas) - label

        risk = XBY.transpose().dot(XBY)/2/n
#        print(i, risk)
        bethas = bethas - alpha/n*XBY.dot(X)


    with open(output_file, 'a', newline='') as f:
        a = csv.writer(f, delimiter=',')
        a.writerow((alpha, iteraciones, bethas[0], bethas[1], bethas[2]))

        
    return bethas
    
    
input_file, output_file = sys.argv[1:]

Data=np.genfromtxt(input_file, delimiter=',')
n = Data.shape[0] #number of samples

X =(Data[:,:2] -Data[:,:2].mean(axis=0))/Data[:,:2].std(axis=0)
X = np.c_[ np.ones(n), X ] #Add ones first column

#Data = pd.read_csv(input_file, delimiter=',')

label = Data[:, 2]




alphas = [0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1, 5, 10]

#alphas = [0.001,0.1, .05,0.1, 0.5,1]

for alpha in alphas:
    betas = log_regresion(X, label, alpha, 100)    
    print(alpha, betas)    
#    print("Predic", X.dot(betas))
#    print("Real  ", label)
    print("Error ", np.abs(X.dot(betas)-label).sum() )
    print("******************")

#My choice
betas = log_regresion(X, label, 1, 48) 


    